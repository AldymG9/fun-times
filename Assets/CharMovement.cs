﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharMovement : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float movementSpeed =10f;
        //Detect when the D arrow key is pressed down
        if (Input.GetKey(KeyCode.D))
        {
            Debug.Log("D key was pressed.");
            transform.position = transform.position + new Vector3(1 * movementSpeed * Time.deltaTime, 0);
        } else if (Input.GetKey(KeyCode.A))
        {
            Debug.Log("A key was pressed.");
            transform.position = transform.position + new Vector3(-1 * movementSpeed * Time.deltaTime, 0);
        }
        if (Input.GetKey(KeyCode.W))
        {
            Debug.Log("W key was pressed.");
            transform.position = transform.position + new Vector3(0, 1 * movementSpeed * Time.deltaTime);
        }
    }
}
